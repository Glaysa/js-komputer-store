
# Komputer Store

A basic website created using HTML, CSS + Bootstrap and JavaScript with the goal to learn the fundamentals of JavaScript.

## Table of Contents

- [Usage](#usage)
- [Screenshot](#screenshots)
- [Technologies](#technologies)
- [Functionalities](#functionalities)
- [Usage](#usage)
- [Authors](#authors)

## Usage

The website is deployed in [Heroku](https://glaysa-komputer-store.herokuapp.com/).

## Screenshots

![screenshot](https://user-images.githubusercontent.com/56070628/189853700-1b0cb9c2-f316-4878-874d-d4c338932fda.png)

## Technologies

- Visual Studio Code - (Code Editor)
- Figma - (Wireframing tool)
- Bootstrap - (CSS Library)
- Live Server - (VS code extension)

## Functionalities

**Get Loan Button**
- A prompt will popup where a user can type in the amount of loan they want to get.
- Loan will be denied if bank balance is 0 or the amount is greater than the bank balance.
- Loan will also be denied if the user has an existing loan.
- Alert popups are used to notify the user of these events.
- The user is also alerted after a successful loan approval.
- Loan is added to the current bank balance if successfully approved.

**Pay Loan Button**
- If salary is sufficient, the entire loan is deducted from the salary.
- The remainder of the salary will be transferred automatically to the bank.
- An alert popup will notify the user if salary is not sufficient.
- The user is also alerted after a successful loan payment.

**Work Button**
- Increases salary by 100 every time it is clicked.

**Transfer to Bank Button**
- Transfers entire salary to the bank.
- Every time the user transfers salary to the bank, 10% of the salary is used to pay the loan if the user has one.

**Buy Button**
- Enables the user to buy a laptop.
- The user's bank balance is deducted with the price of the laptop.
- The user is alerted if they don't have sufficient balance.
- The user is also alerted after a successful purchase.
- Decreases the laptop stock.
- Will be disabled if laptop stock is 0.

**Laptop Options** (dropdown)
- Is populated with laptop information from the noroff api.
- Updates the UI whenever the value changes.

## Authors

- [Glaysa Fernandez](https://gitlab.com/Glaysa)

