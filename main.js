
//################################################################################### Bank and Loan vars

const loanInfo = document.getElementById("loan-info");
const loanBalance = document.getElementById("loan-balance");
const bankBalance = document.getElementById("bank-balance");
const getLoanBtn = document.getElementById("get-loan-btn");
const payLoanBtn = document.getElementById("pay-loan-btn");
let currentBankBalance = 0;
let currentLoanBalance = 0;

//################################################################################### Work vars

const salary = document.getElementById("work-salary");
const transferToBankBtn = document.getElementById("transfer-to-bank-btn");
const workBtn = document.getElementById("work-btn");
let currentSalary = 0;

//################################################################################### laptop vars

const baseApiUrl = "https://noroff-komputer-store-api.herokuapp.com/";
const laptopSpecs = document.getElementById("laptop-specs");
const laptopOptions = document.getElementById("laptop-options");
const laptopTitle = document.getElementById("laptop-title");
const laptopDescription = document.getElementById("laptop-description");
const laptopPrice = document.getElementById("laptop-price");
const laptopStock = document.getElementById("laptop-stock");
const laptopImage = document.getElementById("laptop-img");
const buyBtn = document.getElementById("buy-btn");
let laptopList = new Array();
let selectedLaptop = undefined;

//################################################################################### Init

// Runs after document is loaded and parsed
document.addEventListener("DOMContentLoaded", async () => {
    displayAmount(currentLoanBalance, loanBalance);
    displayAmount(currentBankBalance, bankBalance);
    displayAmount(currentSalary, salary);
    await getAllLaptops();
});

// An api call that retrieves all laptops
async function getAllLaptops() {
    await fetch(baseApiUrl + "computers")
        .then(response => response.json())                  // Get response
        .then((data) => laptopList.push(...data))           // Populate laptop list array
        .then(() => displayLaptopOptions())                 // Populate the select dropdown: has default value of 1
        .then(() => onSelectLaptop());                      // Selects the first laptop
}

//################################################################################### Event Listeners

getLoanBtn.addEventListener("click", getBankLoan);
payLoanBtn.addEventListener("click", payBankLoan);
workBtn.addEventListener("click", increaseSalary);
transferToBankBtn.addEventListener("click", transferToBank);
buyBtn.addEventListener("click", buyLaptop);
laptopOptions.addEventListener("change", onSelectLaptop);

//################################################################################### Bank and Loan functions

// Adds the specified loan amount to bank balance and sets loan balance.
function getBankLoan() {
    if(currentLoanBalance <= 0) {
        currentLoanBalance += parseInt(prompt("Enter a loan amount: "));
        validateLoan(currentLoanBalance);
        currentBankBalance += currentLoanBalance;
        displayAmount(currentBankBalance, bankBalance);
        displayAmount(currentLoanBalance, loanBalance);
        displayLoan();
        alert(`Loan Approved: ${currentLoanBalance} kr.`);
    } else {
        alert("You need to pay your current loan before getting a new loan.");
    }
}

// Deducts entire loan from salary if sufficient.
// The rest are automatically added to bank balance.
function payBankLoan() {
    if(currentSalary >= currentLoanBalance) {
        currentSalary -= currentLoanBalance;
        currentLoanBalance = 0;
        currentBankBalance += currentSalary;
        currentSalary = 0;
        displayAmount(currentLoanBalance , loanBalance);
        displayAmount(currentBankBalance, bankBalance);
        displayAmount(currentSalary, salary);
        displayLoan();
        alert("Loan Paid!");
    } else {
        alert("Your current salary is not enough to pay your loan.");
    }
}

// Shows/hides loan ui.
function displayLoan(){
    if(currentLoanBalance > 0) {
        loanInfo.classList.remove("d-none");
    } else {
        loanInfo.classList.add("d-none");
    }
}

// Validates the value of the loan amount from the prompt input.
function validateLoan(loanAmount){
    try {
        if(isNaN(loanAmount)) {
            throw new Error("Invalid amount. Please try again.");
        }
        if(currentBankBalance <= 0) {
            throw new Error("Loan denied. Your current bank balance is not sufficient to take a loan.");
        }
        if(loanAmount > currentBankBalance) {
            throw new Error(`Loan denied. The max loan you can get is ${currentBankBalance} kr.`);
        }
    } catch(error) {
        alert(error.message);
        currentLoanBalance = 0;
    }
}

//################################################################################### Work functions

// Increases salary by 100.
function increaseSalary() {
    currentSalary += 100;
    displayAmount(currentSalary, salary);
}

// Transfers salary to bank with a 10% deduction if user has a loan.
function transferToBank() {
    deductLoanFromSalaryDuringTransfer();
    currentBankBalance += currentSalary;
    currentSalary = 0;
    displayAmount(currentLoanBalance, loanBalance);
    displayAmount(currentBankBalance, bankBalance);
    displayAmount(currentSalary, salary);
    displayLoan();
}

// Deducts 10% from salary and loan.
function deductLoanFromSalaryDuringTransfer() {
    if(currentLoanBalance > 0) {
        currentLoanBalance -= currentSalary * 0.1;
        currentSalary -= currentSalary * 0.1;
        currentLoanBalance = (currentLoanBalance < 0) ? 0 : currentLoanBalance;
    }
}

//################################################################################### Laptop btn/select click function

// Allows user to buy a laptop if there is stock and bank balance is sufficient.
function buyLaptop() {
    if(currentBankBalance >= selectedLaptop.price && selectedLaptop.stock > 0) {
        currentBankBalance -= selectedLaptop.price;
        selectedLaptop.stock -= 1;
        laptopStock.innerText = "Stocks: " + selectedLaptop.stock;
        displayAmount(currentBankBalance, bankBalance);
        disableBuyBtn();
        alert(`You now own a "${selectedLaptop.title}".`);
    } else {
        alert("Your current bank balance is not enough to buy this laptop.");
    }
}

// Displays the correct laptop info when the laptop dropdown value changes
function onSelectLaptop() {
    selectedLaptop = laptopList.find(laptop => laptop.id == laptopOptions.value);
    displayLaptopSpecs();
    displayLaptopInfo();
    disableBuyBtn();
}

// Create new <option> tags inside the <select> tag to display dropdown options
function displayLaptopOptions() {
    laptopOptions.innerHTML = "";
    for(let laptop of laptopList) {
        let optionElement = document.createElement("option");
        optionElement.value = laptop.id;
        optionElement.innerText = laptop.title;
        laptopOptions.appendChild(optionElement);
    }
}

// Create new <li> tags inside the <ul> tag to display laptop specs
function displayLaptopSpecs() {
    laptopSpecs.innerHTML = "";
    for(let key in selectedLaptop.specs) {
        let listElement = document.createElement("li");
        listElement.innerText = selectedLaptop.specs[key];
        laptopSpecs.appendChild(listElement);
    }
}

// Sets and displays the other laptop information
function displayLaptopInfo() {
    laptopTitle.innerText = selectedLaptop.title;
    laptopDescription.innerText = selectedLaptop.description;
    laptopStock.innerText = "Stocks: " + selectedLaptop.stock;
    laptopImage.setAttribute("src", baseApiUrl + selectedLaptop.image);
    laptopImage.setAttribute("alt", selectedLaptop.title);
    displayAmount(selectedLaptop.price, laptopPrice);
}

//################################################################################### Helper functions

// Disables the "Buy" btn if laptop stocks is less than or equal to 0.
function disableBuyBtn() {
    if(selectedLaptop.stock <= 0) {
        buyBtn.classList.add("disabled");
    } else {
        buyBtn.classList.remove("disabled");
    }
}

// Displays a formatted number to a specified element.
function displayAmount(number, element) {
    element.innerText = formatToCurrency.format(number);
}

const formatToCurrency = new Intl.NumberFormat("no-NO", {
    style: "currency",
    currency: "NOK",
});
